import './App.scss';
import img from './img/logo.png';
import Main from './component/Main';
import Custom from "./component/Custom";
import Test from "./component/Test";
import H1 from "./component/H1";
import Portfolio from "./component/Portfolio";
import Photo from "./component/Photo";
import Our_team from "./component/Our_team";
import Our_group from "./component/Our_group";
import Our_team2 from "./component/Our_team2";
import Our_pricing from "./component/Our_pricing";
import Form_title from "./component/Form_title";
import Container_form from "./component/Container_form";


function App() {
  return (
    <div className="container">
      <header className="header">
        <div className="nav">
          <img src={img} alt="Vue"/>
             <ul className='Nav_lings'>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Gallery</a></li>
              </ul>
            <img src="./img/xbox-menu.png" alt="" className='menu-btn'/>

            <div className="search">

            </div>
          </div>
      </header>
      <Main />
      <Custom />
      <Test />
        <H1 />
        <Portfolio />
        <Photo />
        <Our_team />
        <Our_group />
        <Our_team2 />
        <Our_pricing />
        <Form_title />
        <Container_form />
    </div>
  );
}

export default App;
