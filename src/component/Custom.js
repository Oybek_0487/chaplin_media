import React from "react";
import '../App.scss';

function Custom() {
    return(
        <div className="custom">
            <div className="custom_box">
                <h1>2K</h1>
                <p>Satisfied Customers</p>
            </div>
            <div className="custom_box">
                <h1>15</h1>
                <p>Export Photographer</p>
            </div>
            <div className="custom_box">
                <h1>5</h1>
                <p>Years Experience</p>
            </div>
        </div>

    );
};
export default Custom;