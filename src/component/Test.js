import React from 'react'
import '../App.scss'
import img from '../img/2rasm.jpg'

function Test() {
    return(
        <div className="text">
            <div className="text_child1">
                <img src={img} alt="#"/>
            </div>
            <div className="text-child2">
                <h1>About US</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dolorem,
                    eveniet fugiat illum
                    impedit ipsum, labore, nam necessitatibus nemo
                     officia pariatur perspiciatis quas quidem quis quisquam quod sunt.
                    A ab ad aperiam
                        asperiores atque debitis dolores esse,
                        est illo inventore ipsum labore, laboriosam magnam mollitia
                    nesciunt numquam odio
                            perferendis quod reprehenderit sapiente
                             temporibus unde vitae voluptatem Ad quisquam sapiente temporibus  voluptas
                                voluptate? perspiciatis quidem?
                </p>
                <a href="#">Lead More</a>
            </div>
        </div>
    );
};
export default Test;