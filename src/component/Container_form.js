import React from "react";
import '../App.scss'
import img from '../img/фотографы-12-папарацци-двойной-группы-17887057.jpg'
function Container_form() {
    return(
        <div className="container_form">
          <form action="">
            <div className="col-75">
                <input type="text" id="fname" name="firstname" placeholder="Enter your name" />
            </div>
            <div className="col-75">
                <input type="text" id="lname" name="lastname" placeholder="Enter your main address" />
            </div>
            <div className="col-75">
                <textarea id="subject" name="subject" placeholder="Massage ..." />
            </div>

                <a href="#">Send</a>
          </form>
          <div className="form_img">
            <img src={img} alt="" />
          </div>
    </div>

    );
};
export default Container_form;