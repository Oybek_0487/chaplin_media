import React from "react";
import '../App.scss'


function Our_pricing() {
    return(
        <div className="our_pricing">
            <div className="our_child">
                <div className="our_child2">
                    <h2>Besic</h2>
                    <p>Perfect for small sessions and
                        Personal Photo shoots with
                        and many purposes
                    </p>
                    <h3>$200</h3>
                    <p>
                        Duration : 3 hour
                        Number of Photos : 100
                        Processing Time : 07 Days
                        Detailed retuching : 80 Photos
                    </p>
                    <a href="#">Buy Now</a>
                </div>
            </div>
            <div className="our_child">
                <div className="our_child2">
                    <h2>Standart</h2>
                    <p>Perfect for a business photo shoot, for
                        close friends or a couple. Suitable for
                        small family
                    </p>
                    <h3>$300</h3>
                    <p>
                        Duration : 5 hour
                        Number of Photos : 200
                        Processing Time : 10 Days
                        Detailed retuching : 150 Photos
                    </p>
                    <a href="#">Buy Now</a>
                </div>
            </div>
            <div className="our_child">
                <div className="our_child2">
                    <h2>Premium</h2>
                    <p>Perfect for large campaines concerts
                        celebrations and weeddings
                    </p>
                    <h3>$500</h3>
                    <p>
                        Duration : 8 hour
                        Number of Photos : Unlimited
                        Processing Time : 15 Days
                        Detailed retuching : 200 Photos
                    </p>

                        <a href="#">Buy Now</a>
                </div>
            </div>
        </div>
    );
};
export default Our_pricing;