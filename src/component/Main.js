import React from "react";
import '../index.scss'
import rasim12 from "../img/qizphoto1.webp"


function Main(){
    return(
        <div className="main">
            <div className="main_child">
                <h1>
                    Change your lens,   Change your story
                </h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
                    consectetur cupiditate dolorem harum inventore, ipsam nesciunt optio
                    quisquam
                </p>
                <a href="">Get Started</a>
            </div>
            <div className="main_child2">
                <img src={rasim12} alt="Vue"/>
            </div>
        </div>
    );
};

export default Main;