import React from "react";
import '../App.scss'
import our1 from '../img/our1.jpg'
import our2 from '../img/our2.jpg'
import our3 from '../img/our3.jpg'

function Our_group() {
    return(
        <div className="our_group">
            <div className="our_child">
                <img src={our1} alt="#" />
                    <h3>Claudio Romano</h3>
                    <p>Senor Photographer</p>
                    <p>Fashion & Weeding Photo Expert</p>
                    <a href="#">See More</a>
            </div>
            <div className="our_child">
                <img src={our2} alt="#" />
                    <h3>Natasha Dykes</h3>
                    <p>Senor Photographer</p>
                    <p>Portrait & Architecture Photo Expert</p>
                    <a href="#">See More</a>
            </div>
            <div className="our_child">
                <img src={our3} alt="#" />
                    <h3>Garett Godsey</h3>
                    <p>Senior Videorapher</p>
                    <p>Nature and animals vedio capture expert</p>
                    <a href="#">See More</a>
            </div>
        </div>
    );
};
export default Our_group;