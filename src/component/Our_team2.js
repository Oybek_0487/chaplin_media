import React from 'react'
import '../App.scss'

function Our_team2() {
    return(
        <div className="our_team2">
            <h1>Our Pricing Plans</h1>
            <p>Pick an account plan that fits your Workflow</p>
        </div>
    );
};
export default Our_team2;