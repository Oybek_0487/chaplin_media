import React from "react";
import '../App.scss';

function Form_title() {
    return(
        <div className="form_title">
            <h2>Get In Touch</h2>
            <p>We are here for you! How can we help</p>
        </div>
    );
};
export default Form_title;