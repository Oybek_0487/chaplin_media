import React from "react";
import '../App.scss'
import grid1 from '../img/grid1.jpg'
import grid2 from '../img/grid2.webp'
import grid3 from '../img/grid3.webp'
import grid4 from '../img/grid4.jpg'
import grid5 from '../img/grid5.jpeg'
import grid6 from '../img/grid6.jpg'
import grid7 from '../img/grid7.png'

function Photo() {
    return(
        <div className="photo">
            <div className="grid_child">
                <img src={grid1} className="grid_1" alt="#" />
                    <img src={grid2} className="grid_2" alt="#" />
            </div>
            <div className="grid_child">
                <img src={grid3} className="grid_3" alt="#" />
                    <img src={grid4} className="grid_4" alt="#" />
                        <img src={grid5} className="grid_5" alt="#" />
            </div>
            <div className="grid_child">
                <img src={grid6} className="grid_6" alt="#" />
                    <img src={grid7} className="grid_7" alt="#" />
            </div>
        </div>
    );
};
export default Photo;
