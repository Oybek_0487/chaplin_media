import React from 'react';
import '../App.scss';

function Portfolio() {
    return(
        <div className="portfolio">
            <div className="portfolio_list">
                <ul>
                    <li><a href="#">All</a></li>
                    <li><a href="#">Portrait</a></li>
                    <li><a href="#">Fashion</a></li>
                    <li><a href="#">Wedding</a></li>
                    <li><a href="#">Architecture</a></li>
                </ul>
            </div>
            <div className="portfolio_lest2">
                <a href="#">See More</a>
            </div>
        </div>
    );
};
export default Portfolio;
