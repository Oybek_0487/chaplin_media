import React from "react";
import '../App.scss'

function Our_team() {
    return(
        <div className="our_team">
            <h1>Meet Our Team</h1>
            <p>Meet our world-renowned photographer</p>
        </div>
    );
};
export default Our_team;
